Source: libjs-webrtc-adapter
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
Build-Depends:
 brotli,
 chai <!nocheck>,
 debhelper-compat (= 13),
 mocha <!nocheck>,
 node-babel-cli (>= 7),
 node-babel-preset-env (>= 7),
 node-rollup-plugin-commonjs (>= 15),
 node-rollup-plugin-node-resolve (>= 11),
 node-sdp (>= 3.0.2~),
 node-sinon <!nocheck>,
 node-sinon-chai <!nocheck>,
 pigz,
 rollup,
 uglifyjs (>= 3),
Standards-Version: 4.6.2
Homepage: https://github.com/webrtcHacks/adapter
Vcs-Browser: https://salsa.debian.org/js-team/libjs-webrtc-adapter
Vcs-Git: https://salsa.debian.org/js-team/libjs-webrtc-adapter.git
Rules-Requires-Root: no

Package: node-webrtc-adapter
Architecture: all
Multi-Arch: foreign
Depends:
 node-sdp (>= 3.0.2~),
 ${misc:Depends},
Description: shim to insulate apps from WebRTC quirks - Node.js library
 adapter.js is a shim to insulate apps
 from spec changes and prefix differences.
 In fact, the standards and protocols used for WebRTC implementations
 are highly stable,
 and there are only a few prefixed names.
 .
 WebRTC (Web Real-Time Communication) is a project
 that provides web browsers and mobile applications
 with real-time communication (RTC)
 via simple application programming interfaces (APIs).
 .
 This package contains adapter.js usable with Node.js.

Package: libjs-webrtc-adapter
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: shim to insulate apps from WebRTC quirks - browser library
 adapter.js is a shim to insulate apps
 from spec changes and prefix differences.
 In fact, the standards and protocols used for WebRTC implementations
 are highly stable,
 and there are only a few prefixed names.
 .
 WebRTC (Web Real-Time Communication) is a project
 that provides web browsers and mobile applications
 with real-time communication (RTC)
 via simple application programming interfaces (APIs).
 .
 This package contains adapter.js directly usable in web browsers.
