#!/usr/bin/make -f

STEMS = adapter adapter_no_global
DOCS = README.md

# normalize output with TAP where possible unless terse requested
ifeq (,$(filter terse,$(DEB_BUILD_OPTIONS)))
MOCHA = mocha --reporter tap
else
MOCHA = mocha --reporter dot --no-colors
endif

%:
	dh $@

# build browser libraries
dist/adapter_core5.js: src/js/adapter_core5.js
	babeljs --no-babelrc --presets @babel/preset-env \
		--source-root src/js/index.js \
		--out-dir dist \
		-- src/js
dist/adapter_core_no_global.js: src/js/adapter_core.js
	cp --force --archive src/js/adapter_core.js dist/adapter_core_no_global.js
	sed --in-place --expression 's/const /var /; s/{window}/{ window: window }/; /^export default/d' dist/adapter_core_no_global.js
out/adapter.js: dist/adapter_core5.js
	rollup --strict --config=debian/rollup.config.js --format=iife --name=adapter --file=$@ $<
out/adapter_no_global.js: dist/adapter_core_no_global.js
	rollup --strict --config=debian/rollup.config.js --format=iife --file=$@ $<
$(patsubst %,debian/js/%.js,$(STEMS)): debian/js/%.js: out/%.js
	mkdir --parents debian/js
	cp --force --target-directory debian/js $<


# optimize JavaScript for browser use
# TODO: include source-map when supported by concatenation
debian/js/%.min.js: debian/js/%.js
	mkdir --parents debian/js
	uglifyjs --compress --mangle \
		--output $@ \
		-- $<

# pre-compress for browser use
%.gz: %
	pigz --force --keep -11 -- $<
	brotli --force --keep --best --suffix=.brotli -- $<

override_dh_auto_build: $(patsubst %,debian/js/%.min.js.gz,$(STEMS))

override_dh_auto_test:
	$(MOCHA) test/unit

override_dh_installdocs:
	dh_installdocs --all -- $(DOCS)

.SECONDARY:
