const resolve = require('@rollup/plugin-node-resolve').nodeResolve;
const commonjs = require('@rollup/plugin-commonjs');

module.exports = {
  plugins: [
    resolve({
      modulePaths: ['/usr/share/nodejs','/usr/lib/nodejs']
    }),
    commonjs()
  ],
};
